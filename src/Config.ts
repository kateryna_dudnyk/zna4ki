export default class Config {
  // Куда развернуть проэкт
  static APP_DIR = 'editor';

  // Стартовые параметры (при инициализации приложения) размеров зон значка
  static initMainZone = 35;
  static initBendZone = 25;
  static initDefaultSize = 56;

  // Массив значков для выбора
  static SIZES = [25, 38, 56, 78];

  // Настройки параметров зон значка
  /*static DIAMETERS_OF_ZONE = new Map([
    [25, 35],
    [38, 50],
    [56, 68],
    [78, 91],
  ]);*/

  // Размер редактора
  static editorSize = 290;
  // Количество пикселей смещения при управлении с клавиатуры
  static pixelMoveFromKeyboard = 5;
  // Цвет блока пипетки когда активна
  static eyeDropperActiveColor = '#ccc';
  // Размер картинки, которая уходит на сервер
  // 921,4 (основная зона) + 368,4 (зона загиба)
  static serverSize = 1290;

  // Ссылка на апи сервера
  static serverURL = 'https://jsonplaceholder.typicode.com/albums/1/photos';
  // Обьязательно ли описание
  static isDiscRequire = true;

  // Максимальное УМЕНЬШЕНИЕ картинки в редакторе
  static imageMinScale = 0.5;
  // максимальное УВЕЛИЧЕНИЕ картинки в редакторе
  static imageMaxScale = 4;
  // Ширина линий обозначающих зону и центр в редакторе
  static lineDepth = 2;

  // Ссылка на серверное API
  static moreQuestions = 'http://localhost:3000/#';

  // Разрешенные файлы для загрузки
  static acceptUploadFile = '.png, .jpg, .gif';
  // Минимальное разрешение (ширина) загружаемых файлов
  static minWidthUploadFile = 600;
  // Минимальное разрешение (вісота) загружаемых файлов
  static minHeightUploadFile = 600;
  // Максимальный размер файла в мегабайтах
  static maxImageSize = 3.5;
}
