import React, {ChangeEvent, useEffect, useRef, useState} from 'react';
import DropButton from './DropButton';
import Circles from './Circles';
import {ColorResult, RGBColor, SketchPicker} from 'react-color';
// @ts-ignore
import Transform from 'react-free-transform';
import '../styles/tr.css';
import '../styles/formTransform.css';
import '../styles/loading.css';
import Config from '../Config';
import ErrorMessage from './ErrorMessage';
import Center from './form/Center';
import EyeDropper from './EyeDropper';
import ResetButton from './form/Reset';
import ScaleSetting from './form/ScaleSetting';
import PositionSetting from './form/PositionSetting';
import SetBadgeSize from './form/SetBadgeSize';
import {ServerDataType} from "../App";
import axios from "axios";

type ImageTransformProps = {
  image: string;
  className: string;
  showSecondArrow(show: boolean): void;
  saveFromServer(data: ServerDataType): void;
}

export type ImageState = {
  photo: string
  x: number,
  y: number,
  height: number;
  width: number;
  scaleX: number;
  scaleY: number;
  angle: number;
  color?: RGBColor;
}

const ImageTransform = (props: ImageTransformProps) => {
  const {className, image, saveFromServer, showSecondArrow} = props;

  const [imageState, setImageState] = useState<ImageState>({
    photo: image,
    x: 0,
    y: 0,
    height: 0,
    width: 0,
    scaleX: 1,
    scaleY: 1,
    angle: 0,
  });
  const [showEye, setShowEye] = useState<boolean>(false);
  const [displaySketchPicker, setDisplaySketchPicker] = useState<boolean>(false);
  const [circleSize, setCircleSize] = useState<number>(Config.editorSize * Config.initBendZone / Config.initMainZone);
  const [preview, setPreview] = useState<boolean>(false);
  const [loader, setLoader] = useState<boolean>(false);
  const [error, setError] = useState<string>('');

  const badgeDesc = useRef<string>('');
  const badgeSize = useRef<number>(Config.initDefaultSize);

  useEffect(
    () => {
      resetEditor(image);
      document.getElementById('transform')!.addEventListener('touchmove', function (event) {
        event.preventDefault();
      }, false);
    },
    []
  );

  // Функция заполнения стейта при помощи нашего base64 фотографии
  const resetEditor = (img: string) => {
    showSecondArrow(true);
    const image = new Image();
    image.src = img;

    image.onload = () => {
      const scaleToEditor = Math.sqrt(image.width * image.width + image.height * image.height) / Config.editorSize;
      const newWidth = image.width / scaleToEditor;
      const newHeight = image.height / scaleToEditor;
      setImageState({
        photo: image.src,
        x: (Config.editorSize - newWidth) / 2,
        y: (Config.editorSize - newHeight) / 2,
        height: newHeight,
        width: newWidth,
        scaleX: 1,
        scaleY: 1,
        angle: 0,
      });
    };
    setPreview(false);
    setError('');
  };

  const onTransformImage = (newState: ImageState) => {
    setImageState({...imageState, ...newState});
  };

  // Функция изменения диаметра кругов
  const changeCircleSize = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!e.target.value) return;
    badgeSize.current = +e.target.value;

/*    const sizeDashed = Config.DIAMETERS_OF_ZONE.get(sizeSelected);
    if (!sizeDashed) return;

    setCircleSize(Config.editorSize * sizeSelected / sizeDashed);*/
  };

  // Функция, которая включает режим предпросмотра
  const previewButton = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!event.target) return;
    const value = event.target.checked;
    setPreview(value);
    showSecondArrow(preview);
  };

  // Описание значка
  const textareaDecs = (event: ChangeEvent<HTMLTextAreaElement>) => {
    let value = event.target.value;
    value = value.replace(/[^\wА-ЯЁа-яё,\s]*/g, '');
    event.target.value = value;
    badgeDesc.current = value;
  };

  // Выбор цвета
  const handleChangeComplete = (color: ColorResult) => {
    setImageState({
      ...imageState,
      color: {...color.rgb}
    });
  };

  // Пипетка
  const handleEyeChangeComplete = (color: RGBColor) => {
    setImageState({
      ...imageState,
      color: {...color}
    });
  };

  // Функция вывода ошибки с блока загрузки "другой картинки"
  const getError = (error: string) => {
    setError(error)
  };

  const handleSave = () => {
    if (Config.isDiscRequire && !badgeDesc.current) {
      setError('Поле описания обьязательно к заполнению!');
      return;
    }
    setError('');
    createBase64FromState()
      .then(imageBase64 => {
        setLoader(true);
        showSecondArrow(false);
        axios.post(Config.serverURL, {
          image: imageBase64,
          size: badgeSize.current,
          desc: badgeDesc.current,
          // Тестовые данные для фейкСервера
          bid: 1,
        }).then(res => {
          saveFromServer({
            url: res.data.image,
            bid: res.data.bid,
          });
        }).catch(error => {
          setError(error.message);
        }).finally(() => {
          setLoader(false);
        });
      }).catch(error => {
      // Если неудалось создать канвас или контекст или не выбран цвет
      setError(error);
    });
  };

  const createBase64FromState = () => {
    return new Promise((resolve, reject) => {
      const canvas = document.createElement('canvas') as HTMLCanvasElement;
      const context = canvas.getContext('2d');
      if (!context) {
        reject('Не удалось получить контекст CANVAS');
        return;
      }
      canvas.width = Config.serverSize;
      canvas.height = Config.serverSize;
      const canvasColor = rgbToString(color);

      if (!canvasColor) {
        if (width + y < Config.editorSize ||
            height + y < Config.editorSize ||
            scaleX * width < Config.editorSize ||
            scaleY * height < Config.editorSize)
        {
          reject('Внимание! Не выбран цвет фона.\nУбедитесь, что в зоне загиба есть изображение или выберите цвет фона');
          return;
        }
      } else {
        context.fillStyle = canvasColor;
        context.fillRect(0, 0, Config.serverSize, Config.serverSize);
      }

      const image = new Image();
      image.src = photo;
      image.onload = () => {
        const scaleBetweenServerAndEditor = Config.serverSize / Config.editorSize;
        if (angle) {
          let localX = (x + width * (1 - scaleX) + (width * scaleX / 2)) * scaleBetweenServerAndEditor;
          let localY = (y + height * (1 - scaleY) + (height * scaleY / 2)) * scaleBetweenServerAndEditor;

          context.translate(localX, localY);
          context.rotate(angle * Math.PI / 180);
          context.translate(-localX, -localY);
        }

        context.drawImage(image,
          (x + width * (1 - scaleX)) * scaleBetweenServerAndEditor,
          (y + height * (1 - scaleY)) * scaleBetweenServerAndEditor,
          width * scaleX * scaleBetweenServerAndEditor,
          height * scaleY * scaleBetweenServerAndEditor,
        );
        if (angle) {
          context.restore();
        }
        resolve(canvas.toDataURL());
        //TODO: для отправки binary просто розкоментируйте рядок ниже
        // resolve(dataURLtoBlob(canvas.toDataURL()));
      };
    });
  };

  // TODO: Метод преобразования base64 в binary
/*  const dataURLtoBlob = (dataURL: string) => {
    let array, binary, i;
    binary = atob(dataURL.split(',')[1]);
    array = [];
    i = 0;
    while (i < binary.length) {
      array.push(binary.charCodeAt(i));
      i++;
    }
    const binaryImage = new Blob([new Uint8Array(array)], {
      type: 'image/png'
    });
    console.log('binary Image', binaryImage);
    return binaryImage;
  };*/

  // Блокируем скрол при нажатии клавишь управления
  document.onkeydown = (e: any) => {
    const NAVIGATION = [37, 38, 39, 40];
    if (!e.target || loader) return;
    if (-1 !== NAVIGATION.indexOf(e.keyCode) && (e.target.nodeName === 'BODY' || e.target.type === 'checkbox'))
      e.preventDefault();

    if (e.target.nodeName !== 'BODY' && e.target.type !== 'checkbox' && e.target.tagName !== 'input') return;
    switch (e.key) {
      case 'ArrowLeft': {
        setImageState({...imageState, x: x - Config.pixelMoveFromKeyboard});
        break;
      }
      case 'ArrowRight': {
        setImageState({...imageState, x: x + Config.pixelMoveFromKeyboard});
        break;
      }
      case 'ArrowDown': {
        setImageState({...imageState, y: y + Config.pixelMoveFromKeyboard});
        break;
      }
      case 'ArrowUp': {
        setImageState({...imageState, y: y - Config.pixelMoveFromKeyboard});
        break;
      }
      default:
        break;
    }
  };

  const {photo, x, y, width, height, scaleX, scaleY, angle, color} = imageState;
  const backgroundColor = rgbToString(color);

  // условие для включенной пипетки при предпросмотре
  const center = (Config.editorSize - circleSize - Config.lineDepth * 2) / 2;
  const previewStyle = (preview || loader)
    ? {
      width: `${circleSize}px`,
      height: `${circleSize}px`,
      top: `${center}px`,
      left: `${center}px`,
      overflow: 'hidden',
      borderRadius: '50%',
      pointerEvents: 'none' as 'none',
      backgroundColor: backgroundColor
    }
    : {backgroundColor: backgroundColor, width: `${Config.editorSize}px`, height: `${Config.editorSize}px`};
  return (
    <div className='transformComponent'
         onTouchEnd={(e: any) => e.target.className !== 'saturation-black' ? setDisplaySketchPicker(false) : null}>
      <div className='transform' id='transform'
           style={{width: `${Config.editorSize}px`, height: `${Config.editorSize}px`}}>
        <Circles size={circleSize} preview={preview || loader}/>
        <div className='transformDiv' style={showEye ? {} : previewStyle}>
          {
            !showEye && <Transform x={preview || loader ? x - center : x}
                                   y={preview || loader ? y - center : y}
                                   width={width}
                                   height={height}
                                   scaleX={scaleX}
                                   scaleY={scaleY}
                                   angle={angle}
                                   onUpdate={(payload: ImageState) => onTransformImage(payload)}
                                   classPrefix={(preview || loader) ? '' : 'tr'}
                                   scaleFromCenter={true}
                                   aspectRatio={true}
                                   translateEnabled={!loader}
                                   scaleEnabled={!loader}
                                   rotateEnabled={false}
                                   scaleMinLimit={Config.imageMinScale}
                                   scaleMaxLimit={Config.imageMaxScale}>
              <img src={photo}
                   alt='transform'
                   className={className}
                   height={height}
                   width={width}/>
            </Transform>
          }
        </div>
        {
          showEye && <canvas
            id="myCanvas"
            width={Config.editorSize}
            height={Config.editorSize}
            style={{backgroundColor: backgroundColor}}
          />
        }
      </div>

      <div className='formTransform' style={{top: Config.editorSize, width: Config.editorSize}}>
        <div className='divsInForm'>
          <label className='container'>Предпросмотр
            <input type='checkbox' id='checkbox' onChange={previewButton} checked={preview} disabled={loader}/>
            <span className='checkmark'/>
          </label>
          <div className='colorButtons'>
            <label htmlFor='background'> Фон: </label>
            <input
              type='button'
              id='backgroundButton'
              style={{backgroundColor: backgroundColor}}
              onClick={() => setDisplaySketchPicker(!displaySketchPicker)}
              disabled={loader}
            />
            {displaySketchPicker
              ? <div onMouseLeave={() => setDisplaySketchPicker(false)}>
                <SketchPicker color={color} onChange={handleChangeComplete}/>
              </div>
              : null
            }
            <EyeDropper
              imageState={imageState}
              onEyeActive={(isActive: boolean) => setShowEye(isActive)}
              onSelected={handleEyeChangeComplete}
              disabled={loader}
            />
          </div>
        </div>

        <Center imageState={imageState} setVertical={onTransformImage} setHorizontal={onTransformImage}
                disabled={loader}/>
        <PositionSetting imageState={imageState} transform={onTransformImage} disabled={loader}/>

        <div className='divsInForm'>
          <ScaleSetting imageState={imageState} transform={onTransformImage} disabled={loader}/>
          <ResetButton onClick={() => resetEditor(imageState.photo)} disabled={loader}/>
        </div>

        <SetBadgeSize changeCircleSize={changeCircleSize} disabled={loader}/>

        <div className='divsInForm flexColumn'>
          <label className='descZnk' htmlFor='descZnk'>Описание значка, слова через запятую:</label>
          <textarea id='descZnk' rows={1} required={true} onChange={textareaDecs} disabled={loader}/>
        </div>

        <div className='defaultTextWarm'>
          Перед созданием значка убедитесь, что всё ровно, нет белых полей и обе зоны заполнены картинкой
        </div>

        <ErrorMessage message={error} redText/>
        {
          loader
            ? <div className='normal save'> Сохранение
              <div className='lds-dual-ring'/>
            </div>
            : <input type='button' className='normal' value='Создать классный значок' onClick={handleSave}/>
        }
        <DropButton className='newDrop' onDrop={resetEditor} onGetError={getError} disabled={loader}>
          Загрузить другую картинку
        </DropButton>
      </div>
    </div>
  )
};

export const rgbToString = (color: RGBColor | undefined, lightly = false) => {
  if (!color) return;
  color.a = (lightly && color.a) ? color.a * 0.5 : color.a;
  return `rgba( ${color.r}, ${color.g}, ${color.b}, ${color.a})`;
};

export default ImageTransform;
