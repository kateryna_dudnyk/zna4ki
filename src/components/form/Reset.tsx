import React from 'react';

type ResetProps = {
  disabled?: boolean,
  onClick: () => void
}

const Reset = (props: ResetProps) => {
  const { disabled, onClick } = props;
  return (
    <input
      type='button'
      className='clearButton'
      value='Сбросить всё'
      onClick={onClick}
      disabled={disabled}
    />
  )
};

export default Reset;
