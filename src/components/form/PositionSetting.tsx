import React, { ChangeEvent } from 'react';
import { ImageState } from '../ImageTransform';

type PositionSettingProps = {
  disabled?: boolean,
  imageState: ImageState,
  transform: (imageState: ImageState) => void,
}

const PositionSetting = (props: PositionSettingProps) => {
  const { disabled, imageState, transform } = props;
  const {x, y, angle} = imageState;

  const setNewState = (object: object) => {
    transform(Object.assign(imageState, object));
  };

  return (
    <div className='divsInForm'>
      Положение:
      <div>
        <label>X:</label>
        <input
          type='number'
          className='XButton disable'
          value={Math.round(x)}
          onChange={(e: ChangeEvent<HTMLInputElement>) => setNewState({x: +e.target.value})}
          disabled={disabled}
        />px,
      </div>
      <div>
        <label>Y:</label>
        <input
          type='number'
          className='YButton disable'
          value={Math.round(y)}
          onChange={(e: ChangeEvent<HTMLInputElement>) => setNewState({y: +e.target.value})}
          disabled={disabled}
        />px,
      </div>
      <div>
        <label>поворот:</label>
        <input
          type='number'
          className='angleButton disable'
          value={Math.round(angle)}
          onChange={(e: ChangeEvent<HTMLInputElement>) => setNewState({angle: +e.target.value})}
          disabled={disabled}
        />°
      </div>
    </div>
  )
};

export default PositionSetting;
