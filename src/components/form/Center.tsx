import React from 'react';
import Config from '../../Config';
import { ImageState } from '../ImageTransform';

type CenterProps = {
  disabled?: boolean
  imageState: ImageState;
  setVertical: (state: ImageState) => void;
  setHorizontal: (state: ImageState) => void;
}

const Center = (props: CenterProps) => {
  const { disabled, imageState, setHorizontal, setVertical } = props;
  const {height, scaleX, width, scaleY} = imageState;

  const handleClickVertical = () => {
    const realHeight = height * (1 - scaleY);
    setVertical({...props.imageState, y: ((Config.editorSize - height) / 2) - (realHeight / 2)})
  };

  const handleClickHorizontal = () => {
    const realWidth = width * (1 - scaleX);
    setHorizontal({...props.imageState, x: ((Config.editorSize - width) / 2) - (realWidth / 2) })
  };

  return (
    <>
      <div className='align divsInForm'>
        Центрировать:
        <input
          type='button'
          className='widthButton'
          value={'По вертикали'}
          onClick={handleClickVertical}
          disabled={disabled}
        />
        <input
          type='button'
          className='heightButton'
          value={'Пo горизонтали'}
          onClick={handleClickHorizontal}
          disabled={disabled}
        />
      </div>
    </>
  )
};

export default Center;
