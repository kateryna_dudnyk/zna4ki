import React, {ChangeEvent} from 'react';
import {ImageState} from '../ImageTransform';
import Config from '../../Config';

type ScaleSettingProps = {
  disabled?: boolean,
  imageState: ImageState,
  transform: (imageState: ImageState) => void,
}

const ScaleSetting = (props: ScaleSettingProps) => {
  const {disabled, imageState, transform} = props;
  const {scaleX, scaleY, x, y, height, width} = imageState;
  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const scale = +event.target.value;
    let newX = (scale / 100 * width - scaleX * width) / 2 + x;
    let newY = (scale / 100 * height - scaleY * height) / 2 + y;
    transform({
      ...imageState,
      scaleX: scale / 100,
      scaleY: scale / 100,
      x: newX,
      y: newY,
    })
  };
  return (
    <div>
      Размер:
      <input type='number'
             max={Config.imageMaxScale * 100}
             min={Config.imageMinScale * 100}
             className='imageSizeButton disable'
             value={Math.max(Math.round(scaleX * 100), Math.round(scaleY * 100))}
             onChange={handleChange}
             disabled={disabled}
      />%
    </div>
  )
};

export default ScaleSetting;
