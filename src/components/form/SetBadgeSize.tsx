import React from 'react';
import Config from '../../Config';

type SetBadgeSizeProps = {
  disabled?: boolean,
  changeCircleSize: (e: React.ChangeEvent<HTMLInputElement>) => void,
}
const SetBadgeSize = (props: SetBadgeSizeProps) => {
  const {changeCircleSize, disabled} = props;
  return (
    <div className='divsInForm'>
      <label className='textSizeZnk'>Размер значка:</label>
      {Config.SIZES.map(size => { return (
        <div id='ck-button' key={size}>
          <label style={{width: Config.editorSize / 6.5}}>
            <input
              type='radio'
              className='ZnkSizeButton'
              name='znkSize'
              value={size}
              onChange={changeCircleSize}
              disabled={disabled}
              defaultChecked={size === Config.initDefaultSize}
            />
            <span>{size}мм</span>
          </label>
        </div>)
      })}
    </div>
  )
}

export default SetBadgeSize;
