import React  from 'react';
import Dropzone from 'react-dropzone';
import '../styles/button.css';
import Config from "../Config";

type DropButtonProps = {
  className: string;
  children: string;
  disabled?: boolean;
  onDrop(img: string): void
  onGetError(error: string): void
};

const DropButton: React.FC<DropButtonProps> = (props: DropButtonProps) => {
  const { className, children, onDrop, onGetError, disabled } = props;
  // Компонент обработки получения фотографии от пользователя
  const acceptedFileTypes = Config.acceptUploadFile;

  // Функция обработки файла
  const handleDrop = (file: File[], rejectedFile: File[]) => {
    // Если неправильный формат то генерируем ошибку
    if (rejectedFile && rejectedFile.length > 0) {
      if (Config.maxImageSize * 1024 * 1024 > rejectedFile[0].size){
        onGetError('Разрешённые форматы: ' + Config.acceptUploadFile);
      } else {
        onGetError(`Максимальный размер файла: ${Config.maxImageSize} Mb`);
      }
    }
    // Если правильнй файл то
    if (file && file.length > 0) {
      // Создаём imageBase64Data
      const currentFile = file[0];
      const myFileReader = new FileReader();
      myFileReader.addEventListener('load', () => {
        // Это будет наш дальнейший src фото
        const myResult = myFileReader.result;
        // Создаём обьект изображения с которого можем считать все нужные параметры
        const image = new Image();
        image.src = myResult as string;

        image.onload = () => {
          // Первым делом проверяем разрешение фотокрафии
          if (image.width >= Config.minWidthUploadFile && image.height >= Config.minHeightUploadFile) {
            // Если всё хорошо, то отправляем родителю наш src фотографии
            onDrop(myResult as string);
          } else {
            // Если неправильное разрешение то генерируем ошибку
            onGetError(`Разрешение картинки должно быть не менее ${Config.minWidthUploadFile}x${Config.minHeightUploadFile}px`);
          }
        };
      });
      myFileReader.readAsDataURL(currentFile as Blob);
    }
  };

  return (
    <Dropzone onDrop={handleDrop} maxSize={Config.maxImageSize * 1024 * 1024} accept={acceptedFileTypes} multiple={false} disabled={disabled}>
      {({getRootProps, getInputProps}) => (
        <>
          <div {...getRootProps()}>
            <input {...getInputProps()} />
            <button className={className}>{children}</button>
          </div>
        </>
      )}
    </Dropzone>
  );
};

export default DropButton;
