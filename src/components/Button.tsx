import React from 'react';
import '../styles/button.css'

type ButtonProps = {
  type: string;
  className: string;
  value: string;
  onClick?: void
}

const Button = (props: ButtonProps) => {

  return (
    <input className={props.className}
           type={props.type}
           value={props.value}/>
  )
};

export default Button;
