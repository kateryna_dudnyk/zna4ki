import React from 'react';
import CSS from 'csstype';

const FirstArrow = () => {
  return (
    <svg style={ArrowStyle} width="60" height="235" viewBox="0 0 45 206" fill="none" xmlns="http://www.w3.org/2000/svg">
      <line x1="1" y1="1" x2="18" y2="1" stroke="#B3B3B3"/>
      <line x1="1" y1="162" x2="1" y2="0.6" stroke="#B3B3B3"/>
      <path
        d="M44.2055 205.456C44.4816 205.456 44.7055 205.232 44.7055 204.956L44.7055 200.456C44.7055 200.179 44.4816 199.956 44.2055 199.956C43.9294 199.956 43.7055 200.179 43.7055 200.456L43.7055 204.456L39.7055 204.456C39.4294 204.456 39.2055 204.679 39.2055 204.956C39.2055 205.232 39.4294 205.456 39.7055 205.456L44.2055 205.456ZM0.718438 162.176L43.852 205.309L44.5591 204.602L1.42554 161.468L0.718438 162.176Z"
        fill="#B3B3B3"
      />
    </svg>
  )
};

const ArrowStyle: CSS.Properties = {
  position: 'absolute',
  zIndex: 28,
  top: '48px',
  left: '8px',
  pointerEvents: 'none'
};

export default FirstArrow;
