import React, {CSSProperties} from 'react';

type SecondArrowProps = {
  display: boolean;
}

const SecondArrow = (props: SecondArrowProps) => {
  return (
    <svg style={props.display? ArrowStyle: BlockDisplay} width="34" height="123" viewBox="0 0 32 116" fill="none">
      <line x1="1.00253" y1="0.500006" x2="7.93054" y2="0.535102" stroke="#B3B3B3"/>
      <line x1="1.0831" y1="84.4" x2="1.0831" y2="0.0314636" stroke="#B3B3B3"/>
      <path
        d="M30.8775 115.416C31.1536 115.421 31.3812 115.201 31.3859 114.925L31.4621 110.426C31.4668 110.15 31.2468 109.922 30.9707 109.917C30.6946 109.913 30.467 110.133 30.4623 110.409L30.3945 114.408L26.3951 114.34C26.119 114.336 25.8913 114.556 25.8867 114.832C25.882 115.108 26.102 115.336 26.3781 115.34L30.8775 115.416ZM0.640506 84.3475L30.5265 115.264L31.2454 114.569L1.35949 83.6525L0.640506 84.3475Z"
        fill="#B3B3B3"
      />
    </svg>
  )
};

const ArrowStyle: CSSProperties = {
  position: 'absolute',
  zIndex: 29,
  top: '106px',
  left: '24px',
  pointerEvents: 'none'
};
const BlockDisplay: CSSProperties = {
  display: 'none'
};

export default SecondArrow;
