import React, {CSSProperties} from 'react';

type ErrorProps = {
  message: string,
  redText?: boolean,
}

const ErrorMessage = (props: ErrorProps) => {
  const { message, redText } = props;
  const formattedText = message.split('\n');
  let key = 1;

  return (

    <>
      {
        message &&
        <div style={redText ? MessageWithRedText : MassageInRedBoard}>
          {
            formattedText.map( text => <p style={{margin: 0}} key={key++}>{text}</p>)
          }
        </div>
      }
    </>
  )
};

export default ErrorMessage;

const MassageInRedBoard: CSSProperties = {
  color: '#721c24',
  backgroundColor: '#f8d7da',
  borderColor: '#f5c6cb',
  opacity: '80%',

  padding: '.5rem 1.25rem',
  marginBottom: '1rem',
  marginTop: '1rem',
  borderRadius: '4px',
  textAlign: 'center'
};
const MessageWithRedText: CSSProperties = {
  textAlign: 'center',
  fontSize: '11px',
  color: 'red',
  margin: '4px',
  transition: 'opacity 0.6s',
};
