import React, {useState} from 'react';
import DropButton from './DropButton';
import ErrorMessage from './ErrorMessage';
import Config from "../Config";

type AddImageButtonProps = {
  onImageAdd(img: string): void
};

const AddImageComponent = (props: AddImageButtonProps) => {
  // Этот компонент создан как изначальный при загрузки приложения
  // Тут мы просим пользователя загрузить нам картинку
  const [error, setError] = useState<string>('');
  const [loader, setLoader] = useState<boolean>(false);

  const onGetDropData = (img: string) => {
    setLoader(true);
    // Получаем валидную фотографию пользователя
    if (img.length > 0) {
      // Передаём фото нашему родителю
      props.onImageAdd(img)
    }
  };

  const getError = (error: string) => {
    setError(error)
  };
  return (
    <>
      <p className="center pd1">Загрузите картинку из которой хотите сделать значок</p>
      {
        loader
          ? <div className='normal'>Загрузка
            <div className='lds-dual-ring'/>
          </div>
          : <DropButton className="normal" onDrop={onGetDropData} onGetError={getError}>
            Выбрать картинку
          </DropButton>
      }
      {error && <ErrorMessage message={error}/>}
      <div className={"center pd1"}>
        <div>{`Разрешение картинки должно быть не менее ${Config.minWidthUploadFile}х${Config.minHeightUploadFile}px`}</div>
        <div>{`Форматы: ${Config.acceptUploadFile.toUpperCase().replace(/\./g, "")}`}</div>
        <div>{`Максимальный размер файла: ${Config.maxImageSize} Mb`}</div>
      </div>
    </>
  )
};

export default AddImageComponent;
