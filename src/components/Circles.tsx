import React, { CSSProperties } from 'react';
import Config from '../Config';

type CirclesProps = {
  size: number,
  preview: boolean
}

const Circles = (props: CirclesProps) => {
  const PreviewFunction = props.preview;

  const Circles =
    <div className='circles'>
      <div className='dashed' style={DashedCircleStyle}> </div>
      <div className='solid' style={setNewStyle(SolidCircleStyle, props.size)}> </div>
      <svg style={CircleCenter} className='cntImage' fill="none" width={Config.editorSize} height={Config.editorSize}>
        <line x1={Config.editorSize * 0.48}
            y1={Config.editorSize * 0.5}
            x2={Config.editorSize * 0.52}
            y2={Config.editorSize * 0.5}
            strokeWidth={Config.lineDepth}/>
      <line x1={Config.editorSize * 0.5}
            y1={Config.editorSize * 0.48}
            x2={Config.editorSize * 0.5}
            y2={Config.editorSize * 0.52}
            strokeWidth={Config.lineDepth}/>
    </svg>
  </div>;

  const Preview =
    <div className='preview' style={ PreviewStyle }>
      {/*<div className='insideShadow' style={setNewStyle(InsideShadow, props.size)}/>*/}
      <div className='outsideShadow' style={setNewStyle(OutsideShadow, props.size)}/>
    </div>;

  // при прогрузке страницы отрисовываем нашли круги с полученных размеров
  return (
    <div>
      {
        !PreviewFunction ? Circles : Preview
      }
    </div>
  )

};

const dashedDiameters = Config.editorSize - 2 * Config.lineDepth;

let DashedCircleStyle: CSSProperties = {
  position: 'absolute',
  zIndex: 20,
  width: `${dashedDiameters}px`,
  height: `${dashedDiameters}px`,
  borderRadius: '50%',
  border: `${Config.lineDepth}px dashed #AAA`,
  pointerEvents: 'none',
};

const SolidCircleStyle: CSSProperties = {
  position: 'absolute',
  zIndex: 20,
  borderRadius: '50%',
  border: `${Config.lineDepth}px solid #AAA`,
  pointerEvents: 'none'
};

const CircleCenter: CSSProperties = {
  pointerEvents: 'none',
  stroke: '#AAA',
  position: 'absolute',
  zIndex: 20,
};

export default Circles;

const setNewStyle = (style: CSSProperties, circle: number) => {
  const startDraw = (Config.editorSize - circle - Config.lineDepth * 2) / 2;
  return {
    ...style,
    width: circle,
    height: circle,
    top: startDraw,
    left: startDraw,
  };
};

export const PreviewStyle: CSSProperties = {
  position: 'absolute',
  zIndex: 20,
  width: `${Config.editorSize}px`,
  height: `${Config.editorSize}px`,
  pointerEvents: 'none',
};

/*
export const InsideShadow: CSSProperties = {
  position: 'absolute',
  boxShadow: 'inset 6px -8px 32px 4px rgba(0,0,0,0.2)',
  borderRadius: '50%',
  pointerEvents: 'none',
};
*/

export const OutsideShadow: CSSProperties = {
  position: 'absolute',
  boxShadow: '-2px 10px 20px -7px',
  borderRadius: '50%',
  pointerEvents: 'none',
};
