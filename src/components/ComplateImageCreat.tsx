import React from 'react';
import { ServerDataType } from '../App';
import Config from "../Config";

type CompletedImageCreatProps = {
  serverData: ServerDataType;
  getMore: () => void;
};

const CompletedImageCreate = (props: CompletedImageCreatProps) => {
  const {url, bid} = props.serverData;
  return (
    <div className="completed">
      <h2 className='congratulation'>Ура, ваш значек создан! :)</h2>
      <div style={{ width: Config.editorSize, height: Config.editorSize}}>
          <img src={url} alt={`image_${bid}`} style={{ width: "100%", height: "100%"}}/>
        </div>
      <input type="button" className='normal m-30' value='Добавить значек в корзину' onClick={() => console.log(`Do something width ${bid} `)}/>
      <input type='button' className='normal reverse' value='Создать ещё один значек' onClick={() => props.getMore()}/>
    </div>
  )
};

export default CompletedImageCreate;
