import React from 'react';
import ImageTransform from './ImageTransform';
import '../styles/imageEditor.css';
import {ServerDataType} from "../App";


type ImageZoneProps = {
  photo: string;
  showSecondArrow(show: boolean): void
  saveFromServer(data: ServerDataType): void;
}

const ImageEditor = (props: ImageZoneProps) => {
  const {photo, saveFromServer, showSecondArrow} = props;

  return (
    <div className='imageEditor'>
      <ImageTransform
        className='image'
        image={photo}
        showSecondArrow={showSecondArrow}
        saveFromServer={saveFromServer}
      />
    </div>
  )
};
export default ImageEditor;

