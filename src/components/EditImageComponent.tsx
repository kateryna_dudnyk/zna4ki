import React, {useState} from 'react';
import HelpText from './HelpText';
import ImageEditor from './ImageEditor';
import SecondArrow from './arrows/SecondArrow';
import FirstArrow from './arrows/FirstArrow';
import {ServerDataType} from "../App";

type EditImageComponentProps = {
  photo: string;
  saveFromServer: (data: ServerDataType) => void;
}

const EditImageComponent = (props: EditImageComponentProps) => {
  const [secondArrow, setSecondArrow] = useState( true);

  const showSecondArrow = (show: boolean) => {
    setSecondArrow(show);
  };

  return (
    <div className='editImage'>
      <HelpText/>
      <FirstArrow/>
      <SecondArrow display={secondArrow}/>
      <ImageEditor photo={props.photo} showSecondArrow={showSecondArrow} saveFromServer={props.saveFromServer}/>
    </div>
  )
};

export default EditImageComponent;
