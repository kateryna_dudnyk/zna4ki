import React from 'react';
import '../styles/helpText.css'
import Config from '../Config';

const HelpText = () => {

  return (
    <div className='helpText'>
      <p className='header'>Выровните картинку на значке</p>

      <div className='p_margin'>
        <p className='first'>1. Основная зона значка</p>
        <label className='secondary'>Это будет видно спереди на значке, когда вы его носите. Не оставляйте былых полей -
          это ужасно.</label>
      </div>

      <div className='p_margin'>
        <p className='first'>2. Зона загиба значка</p>
        <label className='secondary'>Это будет видно сбоку значка. Если вы оставите белый фон, то значок может быть не
          красивым. Заполните зону фоном или картинкой.</label>
      </div>

      <div className='p_margin'>
        <label className='first'> 3. <a target={"_blank"} href={Config.moreQuestions}>Посмотреть другие советы </a></label>
      </div>
    </div>
  )
};
export default HelpText;
