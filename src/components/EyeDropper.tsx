import React from 'react';
import EyeDropperSvg from './arrows/EyeDropperSvg';
import { RGBColor } from 'react-color';
import { ImageState } from './ImageTransform';
import Config from '../Config';

type EyeDropperProps = {
  disabled?: boolean,
  imageState: ImageState,
  onEyeActive: (isActive: boolean) => void
  onSelected: (color: RGBColor) => void,
}

const EyeDropper = (props: EyeDropperProps) => {
  const { onEyeActive, onSelected, imageState, disabled } = props;
  const element = document.getElementById('eyeDropper') as HTMLDivElement;

  const ActiveEyeDropper = (e: any) => {
    onEyeActive(true);
    element.style.backgroundColor= Config.eyeDropperActiveColor;
    window.setTimeout( () => {
      const canvas = document.getElementById('myCanvas') as HTMLCanvasElement;
      if (!canvas) {
        return;
      }
      const context = canvas.getContext('2d');
      if (!context) {
        return;
      }

      const image = new Image();
      image.src = imageState.photo;
      image.onload = () => {
        const scaleToEditor = Math.sqrt(image.width * image.width + image.height * image.height) / Config.editorSize;
        const newWidth = image.width / scaleToEditor;
        const newHeight = image.height / scaleToEditor;

        if (imageState.angle) {
          // Смещаем центр канваса для поворота картинки
          //           (           преобразование начала координат             ) + (        половона размера картинки       )
          let localX = imageState.x + imageState.width * (1 - imageState.scaleX) + ( imageState.width * imageState.scaleX / 2);
          let localY = imageState.y + imageState.height * (1 - imageState.scaleY) + ( imageState.height * imageState.scaleY / 2);

          // смещение начала координат в центр картинки
          context.translate(localX, localY);

          // сам поворот в радианах
          context.rotate(imageState.angle*Math.PI/180);

          // возврат центра в левый верхний угол
          context.translate(-localX, -localY);
        }

        context.drawImage(image,
          imageState.x + imageState.width * (1 - imageState.scaleX),
          imageState.y + imageState.height * (1 - imageState.scaleY),
          newWidth * imageState.scaleX,
          newHeight * imageState.scaleY,
        );
        if (imageState.angle) {
          context.restore();
        }
        document.addEventListener("mousemove", GetColor);
        document.addEventListener("touchmove", GetColor);
        document.addEventListener("touchstart", GetColor);
        document.addEventListener("click", removeListener);
        document.addEventListener("touchend", removeListener);
      }
    }, 0);
  };

  // TODO: Понять что тут за ивент! А то не видит свойства nodeName или tagName
  const GetColor = (e: any) => {
    if (e.target.tagName === 'CANVAS') {
      document.body.style.cursor = 'crosshair';
      const canvas = document.getElementById('myCanvas') as HTMLCanvasElement;
      const context: any = canvas.getContext('2d');
      let x, y;
      if (e.type === 'mousemove'){
        x = e.layerX;
        y = e.layerY;
      } else {
        const bcr = e.target.getBoundingClientRect();
        x = e.targetTouches[0].clientX - bcr.x;
        y = e.targetTouches[0].clientY - bcr.y;
      }
      const pixel = context.getImageData(x, y, 1, 1);
      onSelected({
        r: pixel.data[0],
        g: pixel.data[1],
        b: pixel.data[2],
        a: pixel.data[3] / 255,
      });
    } else {
      document.body.style.cursor = 'auto';
    }
  };

  const removeListener = () => {
    onEyeActive(false);
    document.body.style.cursor = 'auto';
    document.removeEventListener("mousemove", GetColor);
    document.removeEventListener("touchmove", GetColor);
    document.removeEventListener("touchstart", GetColor);
    document.removeEventListener("click", removeListener);
    document.removeEventListener("touchend", removeListener);
    element.style.backgroundColor= '';
  };

  return (
    <>
    <button id='eyeDropper' onClick={ActiveEyeDropper} disabled={disabled}>
      <EyeDropperSvg/>
    </button>
    </>
  )
};

export default EyeDropper;
