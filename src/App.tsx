import React, { useState } from 'react';
import AddImageComponent from './components/AddImageComponent';
import EditImageComponent from "./components/EditImageComponent";
import Config from './Config';
import CompletedImageCreate from './components/ComplateImageCreat';


export type ServerDataType = {
  url: string,
  bid: number | null,
}

const App = () => {
  // Этот компонент создан для сбора приложения. Тут он с начального
  // компонента = загрузки изображения передаёт его в компонент редактирования

  const [photo, setPhoto] = useState<string>('');
  const [serverData, setServerData] = useState<ServerDataType>({
    url: '',
    bid: null,
  });

  const addImage = (src: string) => {
    setPhoto(src);
  };

  const handleServerUrl = (data: ServerDataType) => {
    setServerData(data);
  };

  const getMore = () => {
    setServerData({url: '', bid: null});
    setPhoto('');
  };

  return (
    <div className="content" style={{width: Config.editorSize + 10}}>
      {
        serverData.url
          ? <CompletedImageCreate serverData={serverData} getMore={getMore}/>
          : photo.length === 0
            ? <AddImageComponent onImageAdd={addImage}/>
            : <EditImageComponent photo={photo} saveFromServer={handleServerUrl}/>
      }
    </div>
  )
};

export default App;

