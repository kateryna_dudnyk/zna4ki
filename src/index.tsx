import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Config from "./Config";

ReactDOM.render(<App />, document.getElementById(Config.APP_DIR));
serviceWorker.unregister();
